#+AUTHOR: Aman Verma
#+EMAIL: amanv1999@gmail.com	
#+TAGS: READ WRITE DEV MEETING EVENT
* GOALS
** Programming Languages
*** CSS
**** Web Design [0/7]
     :PROPERTIES:
     :ESTIMATED: 42
     :ACTUAL:
     :OWNER: nightwarrior-xxx
     :ID: READ.1539457010
     :TASKID: READ.1539457010
     :END:
      - [ ] Basic CSS                                                                                                 (6h)
        [[https://learn.freecodecamp.org/responsive-web-design/basic-css]]
      - [ ] Applied Visual Design                                                                                     (6h)
	[[https://learn.freecodecamp.org/responsive-web-design/applied-visual-design]]
      - [ ] Applied Accessibility                                                                                     (6h)
        [[https://learn.freecodecamp.org/responsive-web-design/applied-accessibility]]
      - [ ] Responsive Web Design Principles                                                                          (10h)
        [[https://learn.freecodecamp.org/responsive-web-design/responsive-web-design-principles]]
      - [ ] CSS Flexbox		                                                                                             (8h)
        [[https://learn.freecodecamp.org/responsive-web-design/css-flexbox]]
      - [ ] CSS Grid			                                                                                             (6h)
        [[https://learn.freecodecamp.org/responsive-web-design/css-grid]]
*** JavaScript
**** Javascript Algorithms and Data Structures  [/]
     :PROPERTIES:
     :ESTIMATED: 42
     :ACTUAL:
     :OWNER: nightwarrior-xxx
     :ID: READ.1539457208
     :TASKID: READ.1539457208
     :END:  
      - [ ] Basic JavaScript                                                                                          (6h)
        [[https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/basic-javascript]]   
      - [ ] ES6					                                                                                              (6h)
        [[https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/es6]]
      - [ ] Regular Expressions		                                                                                    (3h)
        [[https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/regular-expressions]]
      - [ ] Debugging				                                                                                          (3h)
        [[https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/debugging]]
      - [ ] Basic Data Structures	                                                                                    (4h)
        [[https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/basic-data-structures]]
      - [ ] Basic Algorithm Scripting		                                                                              (4h)
        [[https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/basic-algorithm-scripting]]
      - [ ] Object Oriented Programming		                                                                            (6h)
        [[https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/object-oriented-programming]]
      - [ ] Functional Programming		                                                                                (6h)
        [[https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/functional-programming]]
      - [ ] Intermediate Algorithm Scripting                                                                          (6h)
        [[https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/intermediate-algorithm-scripting]]
*** Python
**** PyBites	[0/53]
     - [ ] 53 - Query the Spotify API 					()
     - [ ] 52 - Create your own Pomodoro Timer				()
     - [ ] 51 - Analyse NBA Data with SQL/sqlite3			()
     - [ ] 50 - Use Celery to Offload an Expensive Task			()
     - [ ] 49 - Contribute to Open Source: Clean up Planet Python	()
     - [ ] 48 - Create a Python News Digest Tool			()
     - [ ] 47 - PyBites First Year in Data (Special)			()
     - [ ] 46 - Add Continuous Integration (CI) to Your Project		()
     - [ ] 45 - TDD: Code FizzBuzz Writing Tests First!			()	
     - [ ] 44 - Marvel Data Analysis (Alicante PyChallengeDay)		()
     - [ ] 43 - Build a Chatbot Using Python				()		
     - [ ] 42 - Mastering Regular Expressions				()
     - [ ] 41 - Daily Python Tip Part 2 - Build an API			()
     - [ ] 40 - Daily Python Tip Part 1 - Make a Web App		()
     - [ ] 39 - Writing Tests With Pytest				()
     - [ ] 38 - Build Your Own Hacktoberfest Checker With Bottle	()
     - [ ] 37 - Automate a Task With Twilio				()
     - [ ] 36 - Create an AWS Lambda Function				()
     - [ ] 35 - Improve Your Python Code With BetterCodeHub		()
     - [ ] 34 - Build a Simple API With Django REST Framework		()
     - [ ] 33 - Build a Django Tracker, Weather or Review App		()
     - [ ] 32 - Test a Simple Django App With Selenium			()
     - [ ] 31 - Image Manipulation With Pillow				()
     - [ ] 30 - The Art of Refactoring: Improve Your Code		()
     - [ ] 29 - Create a Simple Django App				()
     - [ ] 28 - Integrate a Bokeh Chart Into Flask			()
     - [ ] 27 - PRAW: The Python Reddit API Wrapper			()
     - [ ] 26 - Create a Simple Python GUI				()
     - [ ] 25 - Notification Service of Now Playing and Upcoming Movies	()
     - [ ] 24 - Use Dunder / Special Methods to Enrich a Class		()
     - [ ] 23 - Challenge Estimated Time API				()
     - [ ] 22 - Packt Free Ebook Web Scraper				()
     - [ ] 21 - Electricity Cost Calculation App			()
     - [ ] 20 - Object Oriented Programming Fun				()
     - [ ] 19 - Post to Your Favorite API				()
     - [ ] 18 - Get Recommendations					()
     - [ ] 17 - Never Miss a Good Podcast				()
     - [ ] 16 - Query Your Favorite API					()
     - [ ] 15 - Create a Simple Flask App				()
     - [ ] 14 - Write DRY Code With Decorators				()
     - [ ] 13 - Highest Rated Movie Directors				()
     - [ ] 12 - Build a Tic-tac-toe Game				()
     - [ ] 11 - Generators for Fun and Profit				()
     - [ ] 10 - Build a Hangman Game					()
     - [ ] 09 - The With Statement and Context Managers			()
     - [ ] 08 - House Inventory Tracker					()
     - [ ] 07 - Twitter Sentiment Analysis				()
     - [ ] 06 - PyPI 100K Packages Prediction				()
     - [ ] 05 - Twitter data analysis Part 2: Similar Tweeters		()
     - [ ] 04 - Twitter data analysis Part 1: Getting Data		()
     - [ ] 03 - PyBites Blog Tag Analysis				()
     - [ ] 02 - Word Values Part II - A Simple Game			()
     - [ ] 01 - Word Values Part I					()   
** Web Frameworks
*** Front End Libraries [/] 
**** Bootstrap [/]
     [[https://learn.freecodecamp.org/front-end-libraries/bootstrap]]
      - [ ] Responsive Design with Bootstrap Fluid Containers()
      - [ ] Make Images Mobile Responsive()
      - [ ] Center Text with Bootstrap()
      - [ ] Create a Bootstrap Button ()
      - [ ] Create a Block Element Bootstrap Button()
      - [ ] Taste the Bootstrap Button Color Rainbow()
      - [ ] Call out Optional Actions with btn-info()
      - [ ] Warn Your Users of a Dangerous Action with btn-danger()
      - [ ] Use the Bootstrap Grid to Put Elements Side By Side()
      - [ ] Ditch Custom CSS for Bootstrap()
      - [ ] Use a span to Target Inline Elements()
      - [ ] Create a Custom Heading()
      - [ ] Add Font Awesome Icons to our Buttons()
      - [ ] Add Font Awesome Icons to all of our Buttons()
      - [ ] Responsively Style Radio Buttons()
      - [ ] Responsively Style Checkboxes()
      - [ ] Style Text Inputs as Form Controls()
      - [ ] Line up Form Elements Responsively with Bootstrap()
      - [ ] Create a Bootstrap Headline()
      - [ ] House our page within a Bootstrap container-fluid div()
      - [ ] Create a Bootstrap Row()
      - [ ] Split Your Bootstrap Row()
      - [ ] Create Bootstrap Wells()
      - [ ] Add Elements within Your Bootstrap Wells()
      - [ ] Apply the Default Bootstrap Button Style()
      - [ ] Create a Class to Target with jQuery Selectors()
      - [ ] Add id Attributes to Bootstrap Elements()
      - [ ] Label Bootstrap Wells()
      - [ ] Give Each Element a Unique id()
      - [ ] Label Bootstrap Buttons()
      - [ ] Use Comments to Clarify Code()
**** jQuery [/]
     [[https://learn.freecodecamp.org/front-end-libraries/jquery]]
      - [ ] Learn How Script Tags and Document Ready Work()
      - [ ] Target HTML Elements with Selectors Using jQuery()
      - [ ] Target Elements by Class Using jQuery()
      - [ ] Target Elements by id Using jQuery()
      - [ ] Delete Your jQuery Functions()
      - [ ] Target the Same Element with Multiple jQuery Selectors()
      - [ ] Remove Classes from an Element with jQuery()
      - [ ] Change the CSS of an Element Using jQuery()
      - [ ] Disable an Element Using jQuery()
      - [ ] Change Text Inside an Element Using jQuery()
      - [ ] Remove an Element Using jQuery()
      - [ ] Use appendTo to Move Elements with jQuery()
      - [ ] Clone an Element Using jQuery()
      - [ ] Target the Parent of an Element Using jQuery()
      - [ ] Target the Children of an Element Using jQuery()
      - [ ] Target a Specific Child of an Element Using jQuery()
      - [ ] Target Even Elements Using jQuery()
      - [ ] Use jQuery to Modify the Entire Page()
**** SASS [0/9]
      [[https://learn.freecodecamp.org/front-end-libraries/sass]]
      - [ ] Store Data with Sass Variables()
      - [ ] Nest CSS with Sass()
      - [ ] Create Reusable CSS with Mixins()
      - [ ] Use @if and @else to Add Logic To Your Styles()
      - [ ] Use @for to Create a Sass Loop()
      - [ ] Use @each to Map Over Items in a List()
      - [ ] Apply a Style Until a Condition is Met with @while()
      - [ ] Split Your Styles into Smaller Chunks with Partials()
      - [ ] Extend One Set of CSS Styles to Another Element()
**** React [/]
      [[https://learn.freecodecamp.org/front-end-libraries/react]]
    - [ ]  Create a Simple JSX Element()
    - [ ] Create a Complex JSX Element()
    - [ ] Add Comments in JSX()
    - [ ] Render HTML Elements to the DOM()
    - [ ] Define an HTML Class in JSX()
    - [ ] Learn About Self-Closing JSX Tags()
    - [ ] Create a Stateless Functional Component()
    - [ ] Create a React Component()
    - [ ] Create a Component with Composition()
    - [ ] Use React to Render Nested Components()
    - [ ] Compose React Components()
    - [ ] Render a Class Component to the DOM()
    - [ ] Write a React Component from Scratch()
    - [ ] Pass Props to a Stateless Functional Component()
    - [ ] Pass an Array as Props()
    - [ ] Use Default Props()
    - [ ] Override Default Props()
    - [ ] Use PropTypes to Define the Props You Expect()
    - [ ] Access Props Using this.props()
    - [ ] Review Using Props with Stateless Functional Components()
    - [ ] Create a Stateful Component()
    - [ ] Render State in the User Interface()
    - [ ] Render State in the User Interface Another Way()
    - [ ] Set State with this.setState()
    - [ ] Bind'this' to a Class Method()
    - [ ] Use State to Toggle an Element()
    - [ ] Write a Simple Counter()
    - [ ] Create a Controlled Input()
    - [ ] Create a Controlled Form()
    - [ ] Pass State as Props to Child Components()
    - [ ] Pass a Callback as Props()
    - [ ] Use the Lifecycle Method componentWillMount()
    - [ ] Use the Lifecycle Method componentDidMount()
    - [ ] Add Event Listeners()
    - [ ] Manage Updates with Lifecycle Methods()
    - [ ] Optimize Re-Renders with shouldComponentUpdate()
    - [ ] Introducing Inline Styles()
    - [ ] Add Inline Styles in React()
    - [ ] Use Advanced JavaScript in React Render Method()
    - [ ] Render with an If/Else Condition()
    - [ ] Use && for a More Concise Conditional()
    - [ ] Use a Ternary Expression for Conditional Rendering()
    - [ ] Render Conditionally from Props()
    - [ ] Change Inline CSS Conditionally Based on Component State()
    - [ ] Use Array.map() to Dynamically Render Elements()
    - [ ] Give Sibling Elements a Unique Key Attribute()
    - [ ] Use Array.filter() to Dynamically Filter an Array()
    - [ ] Render React on the Server with renderToString() [[https://docs.djangoproject.com/en/2.1/intro/]]
*** Backend FrameWorks  [/]
**** Node [/]
     - [ ] Node.js
     - [ ] Npm
     - [ ] Asynchronous programming
     - [ ] ES6/ES7
     - [ ] MongoDB
     - [ ] Express
     - [ ] Socket.IO
     - [ ] Jwt Authentication
     - [ ] Mongoose
     - [ ] Application deployment with Heroku
     - [ ] Version control with Git
     - [ ] GitHub
     - [ ] REST API Design
     - [ ] Code testing
     - [ ] Debugging
     - [ ] Mocha
** Databases
*** SQL [0/4] 
     - [ ] Basic SQL
       [[https://www.w3schools.com/sql/]]
     - [ ] Join SQL
       [[http://iips.icci.edu.iq/images/exam/databases-ramaz.pdf]]
       [[Jiit.ac.in/slides]]/
      - [ ] Nested SQL
       [[http://iips.icci.edu.iq/images/exam/databases-ramaz.pdf]]
       [[Jiit.ac.in/slides/]]
     - [ ] PL SQL 
       [[http://iips.icci.edu.iq/images/exam/databases-ramaz.pdf]]
       [[Jiit.ac.in/slides/]]
** Data Structures And Algorithms
*** HackerEarth
    [[https://www.hackerearth.com/practice/codemonk/]]
**** Practice Problems [/]
       - [ ] Basics of Programming				()
       - [ ] Arrays					            	()
       - [ ] Strings					          	()
       - [ ] Sorting					          	()
       - [ ] Searching					        	()
       - [ ] Stacks					            	()
       - [ ] Queues					            	()
       - [ ] STL					              	()
       - [ ] Number Theory-I				    	()
       - [ ] Trees				            		()
       - [ ] Graph Theory-I				      	()
       - [ ] Hashing					          	()
       - [ ] Disjoint-Set Union				    ()
       - [ ] Heaps					            	()
       - [ ] Priority Queues				    	()
       - [ ] Greedy Technique				    	()
       - [ ] Graph Theory-II					    ()
       - [ ] Dynamic Programming				  ()
       - [ ] Bit Manipulation					    ()
*** Euler Project
     [[https://www.hackerrank.com/contests/projecteuler/challenges]]
**** Practice Problems [0/23]
       - [ ] Multiples of 3 and 5	                        	()
       - [ ] Even Fibonacci numbers				                	()
       - [ ] Largest prime factor				                   	()
       - [ ] Largest palindrome product				             	()
       - [ ] Smallest multiple					                  	()
       - [ ] Sum square difference					                ()	
       - [ ] 10001st prime						                      ()
       - [ ] Largest product in a series				            ()	
       - [ ] Special Pythagorean triplet				            ()	
       - [ ] Summation of primes					                  ()
       - [ ] Largest product in a grid					            ()
       - [ ] Highly divisible triangular number 			      ()	
       - [ ] Large sum							                        ()
       - [ ] Longest Collatz sequence 					            ()	
       - [ ] Lattice paths						                      ()
       - [ ] Power digit sum						                    ()
       - [ ] Number letter counts 					                ()	
       - [ ] Maximum path sum I						                  ()
       - [ ] Counting Sunday						                    ()
       - [ ] Factorial digit sum					                  ()
       - [ ] Amicable numbers						                    ()
       - [ ] Names scores						                        ()
       - [ ] Non-abundant sums						                  ()
** Shell [/]
*** Advance Shell Scripting By TLDP (40h)  
   :PROPERTIES:
   :ESTIMATED: 20h
   :ACTUAL:
   :OWNER: nightwarrior-xxx
   :ID: READ.1539458077
   :TASKID: READ.1539458077
   :END:
     [[http://tldp.org/LDP/abs/html/]]
    - [ ] cleanup: A script to clean up log files in /var/log
    - [ ] cleanup: An improved clean-up script
    - [ ] leanup: An enhanced and generalized version of above scripts.
    - [ ] Code blocks and I/O redirection
    - [ ] Saving the output of a code block to a file
    - [ ] Running a loop in the background
    - [ ] Backup of all files changed in last day
    - [ ] Variable assignment and substitution
    - [ ] Plain Variable Assignment
    - [ ] Variable Assignment, plain and fancy
    - [ ] Integer or string?
    - [ ] Positional Parameters
    - [ ] whois domain name lookup
    - [ ] Using shift
    - [ ] Echoing Weird Variables
    - [ ] Escaped Characters
    - [ ] Detecting key-presses
    - [ ] exit / exit status
    - [ ] Negating a condition using !
    - [ ] What is truth?
    - [ ] Equivalence of test, /usr/bin/test, [ ], and /usr/bin/[
    - [ ] Arithmetic Tests using (( ))
    - [ ] Testing for broken links
    - [ ] Arithmetic and string comparisons
    - [ ] Testing whether a string is null
    - [ ] zmore
    - [ ] Greatest common divisor
    - [ ] Using Arithmetic Operations
    - [ ] Compound Condition Tests Using && and ||
    - [ ] Representation of numerical constants
    - [ ] C-style manipulation of variables
    - [ ] $IFS and whitespace
    - [ ] Timed Input
    - [ ] Once more, timed input
    - [ ] Timed read
    - [ ] Am I root?
    - [ ] arglist: Listing arguments with $* and $@
    - [ ] Inconsistent $* and $@ behavior
    - [ ] $* and $@ when $IFS is empty
    - [ ] Underscore variable
    - [ ] Using declare to type variables
    - [ ] Generating random numbers
    - [ ] Picking a random card from a deck
    - [ ] Brownian Motion Simulation
    - [ ] Random between values
    - [ ] Rolling a single die with RANDOM
    - [ ] Reseeding RANDOM
    - [ ]  Pseudorandom numbers, using awk
    - [ ]  Inserting a blank line between paragraphs in a text file
    - [ ] Generating an 8-character "random" string      
** System
*** Kernel and Drivers
    [[http://www.tldp.org/LDP/lkmpg/2.6/lkmpg.pdf]]
** Information Security(Top Prority) 
*** ShellCoders HandBooks [%] 
     :PROPERTIES:
     :ESTIMATED: 1000h
     :ACTUAL:
     :OWNER: nightwarrior-xxx
     :ID: READ.1539882909
     :TASKID: READ.1539882909
     :END:
** Meetups
*** Csaw IIT-Kanpur
    :PROPERTIES:
    :ESTIMATED: 6
    :ACTUAL:
    :OWNER: nightwarrior-xxx
    :ID: EVENT.1539494237
    :TASKID: EVENT.1539494237
    :END:
*** Pydelhi
    :PROPERTIES:
    :ESTIMATED: 6
    :ACTUAL:
    :OWNER: nightwarrior-xxx
    :ID: MEETING.1539494315
    :TASKID: MEETING.1539494315
    :END:
*** Ilug-D
    :PROPERTIES:
    :ESTIMATED: 6
    :ACTUAL:
    :OWNER: nightwarrior-xxx
    :ID: MEETING.1539494348
    :TASKID: MEETING.1539494348
    :END:
*** Dgplug
    :PROPERTIES:
    :ESTIMATED: 2
    :ACTUAL:
    :OWNER: nightwarrior-xxx
    :ID: MEETING.1539494419
    :TASKID: MEETING.1539494419
    :END:
*** PyCon
    :PROPERTIES:
    :ESTIMATED: 8
    :ACTUAL:
    :OWNER: nightwarrior-xxx
    :ID: EVENT.1539494482
    :TASKID: EVENT.1539494482
    :END:
** Projects
*** Mozilla  Firefox
*** Secure Drop (Top Prority)
*** Blog
*** Banking System
*** E-commerce site
*** CSS Projects
**** Responsive Web Design Projects [/]
     [[https://learn.freecodecamp.org/responsive-web-design/responsive-web-design-projects]]
*** Javascripts Projects 
**** WesBos [0/30]
     [[https://javascript30.com/]]
**** JavaScript Algorithms and Data Structures Projects [/] 
     [[https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/javascript-algorithms-and-data-structures-projects]]
     - [ ] Palindrome Checker
     - [ ] Roman Numeral Converter
     - [ ] Caesars Cipher
     - [ ] Telephone Number Validator
     - [ ] Cash Register 
* PLAN:
